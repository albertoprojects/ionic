import {Injectable} from '@angular/core';
import {DataProvider} from './data/data';
import { FCM } from '@ionic-native/fcm';
import { AuthService} from './auth.service';

@Injectable()
export class FCMService {

    token: string = null;
    deviceId: string = null;
    userId: string = null;
  constructor(private fcm: FCM,
    private authService: AuthService,
    private dataProvider: DataProvider) {
        //this.initPush();
    }

   public initPush() {
    this.authService.user.take(1).subscribe(user => {
      this.userId = user.uid;

      this.fcm.subscribeToTopic('all');

      this.fcm.getToken().then(token => {
        this.updateToken(token);
      });
      
      this.fcm.onNotification().subscribe(data => {
        if(data.wasTapped){
          console.log("Received in background");
        } else {
          console.log("Received in foreground");
        };
      });
      
      this.fcm.onTokenRefresh().subscribe(token => {
        this.updateToken(token);
      });

    });
      
    }

    
    private updateToken(token) {
      if (this.token != token) {
        this.token = token;
        this.dataProvider.callFunction('updateFCMToken', {userId: this.userId, tokenFCM: token});
      }
    }
 


 


  
} 