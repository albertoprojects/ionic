import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  country?: string;
}


@Injectable()
export class AuthService {

  public user: Observable<User>;
  public userRef: AngularFirestoreDocument<any>;

  constructor(private afAuth: AngularFireAuth,

              private afs: AngularFirestore) {

      //// Get auth data, then get firestore user document || null
      this.user = this.afAuth.authState
        .switchMap(user => {
          if (user) {
            this.userRef = this.afs.doc<User>(`users/${user.uid}`);
            return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
          } else {
            return Observable.of(null)
          }
        })
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }

  twitterLogin() {
    const provider = new firebase.auth.TwitterAuthProvider();
    return this.oAuthLogin(provider);
  }

  credentialsLogin(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  
  emailSignUp(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(user => {
        return this.updateUserData(user) // create initial user document
      });
  }

  // Update properties on the user document
  updateUser(user: User, data: any) { 
    return this.afs.doc(`users/${user.uid}`).update(data)
  }

  logOut() {
    return this.afAuth.auth.signOut().then(()=> {
      Promise.resolve();
    });
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(()=> {
      return this.afAuth.auth.signInWithRedirect(provider).then(() => {
        return firebase.auth().getRedirectResult();
      }).then((credential) =>{
        return this.updateUserData(credential.user);
      });
    });
    /*
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user)
      });
      */
  }


  private updateUserData(user): Promise<any> {
    // Sets user data to firestore on login

    this.userRef = this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    }

    localStorage.setItem('userData', JSON.stringify(data));

    return this.userRef.set(data, { merge: true }).then(user => {
      return Promise.resolve(data);
    });

  }

  /*
  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.navCtrl.goToRoot({});
    });
  }
  */
  
}