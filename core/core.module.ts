import { NgModule } from '@angular/core';
import { FCM } from '@ionic-native/fcm';
import { Geolocation } from '@ionic-native/geolocation'; 
import { NativeGeocoder } from '@ionic-native/native-geocoder';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AuthService } from './auth.service';
import {DataProvider} from './data/data';
import {FirebaseProvider} from './data/firebase';
import { MapsService} from './maps.service';
import {GPSService} from './gps.service';
import {FCMService} from './fcm.service';

import {CoreComponentsModule} from './components/core.components.module';

@NgModule({
  imports: [
    AngularFireAuthModule,
    AngularFirestoreModule,
    CoreComponentsModule
  ],
  providers: [AuthService,
     DataProvider, 
     FirebaseProvider, 
     FCMService, 
     FCM,
    Geolocation,
    MapsService,
    GPSService,
    NativeGeocoder],
  exports: [
    
  ]
})
export class CoreModule { }