import {Injectable, ElementRef} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Geolocation, Geoposition} from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';

declare var google;

@Injectable()
export class GPSService {

    private map: any;
    private position: any;
  constructor(private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) {
        
    }
    
    private watchPosition() {
        var options = {
            enableHighAccuracy: true,
            timeout: 30000,
            maximumAge: 3600000
          };
          
        this.position =  this.geolocation.watchPosition(options).filter((p) => p.coords !== undefined);

        
        this.position.subscribe(coords => {
            localStorage.setItem("lastcoords", JSON.stringify(coords));
        });
        
    }
    
    observePosition():  Observable<Geoposition> {
        var options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 10000
          };
        return this.geolocation.watchPosition(options).filter((p) => p.coords !== undefined);
    }
    
    getLastPosition() {
        return JSON.parse(localStorage.getItem("lastcoords") || '{}');
    }

    getCurrentPosition(): Promise<any> {
        var options = {
            enableHighAccuracy: true,
            frequency: 1000
          };
        return this.geolocation.getCurrentPosition(options).then(coords => {
            localStorage.setItem("lascoords", JSON.stringify(coords));
            return Promise.resolve(coords);
        }).catch(error => {
            let lastCoords = localStorage.getItem("lastcoords");
            if (lastCoords) {
                return Promise.resolve(JSON.parse(lastCoords));
            }else {
                return Promise.reject(error);
            }
        });
    }

    getCoordsInfo(latitude, longitude): Promise<any> {
        let reverseoptions: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };
        return this.nativeGeocoder.reverseGeocode(latitude, longitude, reverseoptions)
                                .then((result: NativeGeocoderReverseResult[]) => { return Promise.resolve(result[0]) })
                                .catch((error: any) => { console.log(error); return Promise.reject(error); });
    }

    getLocationInfo(location): Promise<any> {
        let reverseoptions: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };
        return this.nativeGeocoder.forwardGeocode(location, reverseoptions)
                                .then((coordinates: NativeGeocoderForwardResult[]) =>{ return Promise.resolve(coordinates[0]) })
                                .catch((error: any) =>{ console.log(error); return Promise.reject(error); });
    }
}