import { NgModule } from '@angular/core';

import { FilterComponent } from './filter/filter';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    FilterComponent
  ],
  imports: [
    IonicModule
  ],
  exports: [
    FilterComponent
  ]
})
export class CoreComponentsModule { }