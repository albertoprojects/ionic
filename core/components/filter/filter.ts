import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'filter',
  templateUrl: 'filter.html'
})
export class FilterComponent {
  options: any[];
  preselect: string;
  constructor(public navCtrl: NavController,
    private viewCtrl: ViewController,
    private navParamas: NavParams) {
      this.options = this.navParamas.get('options');
      this.preselect = this.navParamas.get('preselect');
  }
  onCancel() {
    this.dismiss(null);
  }
  onFilterSelected($event) {
    this.dismiss($event);
  }
  dismiss(option) {
      this.viewCtrl.dismiss(option);
  }
}
