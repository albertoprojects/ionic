import {Injectable, ElementRef} from '@angular/core';
import {DataProvider} from './data/data';
import { FCM } from '@ionic-native/fcm';
import { AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';
import {Geolocation, Geoposition} from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';


declare var google;

@Injectable()
export class MapsService {

    private map: any;
    private placeSelect: Observable<any>;
  constructor(private fcm: FCM,
    private authService: AuthService,
    private dataProvider: DataProvider,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private platform: Platform) {
        this.initListeners();
    }


    initMap(mapElement: ElementRef, mapOptions: any) {
        this.map = new google.maps.Map(mapElement.nativeElement, mapOptions);
    }

    initListeners() {
        this.placeSelect = new Observable(observer => {
            this.map.addListener('click', ($event) => {
                if ($event.placeId) {
                    this.getPlaceData($event.placeId).then(placeData => {
                        observer.next(placeData);
                    }).catch((status)=> {
                        observer.next(null);
                    });
                }
             })
        });
    }

    onPlaceSelect(): Observable<any> {
        return this.placeSelect ;
    }

    getPlaceData(placeId: any): Promise<any> {
        let service = new google.maps.places.PlacesService(this.map);
        return new Promise ((resolve, reject) => service.getDetails({
            placeId: placeId
        }, (place, status) => {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log(place);
                resolve(place);
            }else {
                reject(status);
            }
        }));
    }

    getAddressInfo(place: any,levels: string[], store: any) {
        place.address_components.forEach((element: any) => {
            levels.forEach(level => {
                let index = element.types.indexOf(level);
                if (index >= 0) {
                  store[level].short_name = element.short_name;
                  store[level].long_name = element.long_name;
                }
            });
        });
      }

} 