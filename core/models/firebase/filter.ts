export type WhereFilterOp = '<' | '<=' | '==' | '>=' | '>';
export interface Filter {
    key?: string,
    condition?: WhereFilterOp,
    value?: any
}