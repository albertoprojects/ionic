export interface GeoQuery {
    center?: {
        lat: number,
        lng: number
    }
    radius?: number;
    field?: string;
}