import { Filter } from './filter';
export interface Query {
    startAt?: number,
    endAt?: number,
    filters?: Filter[],
    orderBy?: string
}