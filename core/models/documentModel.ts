import * as firebase from 'firebase';
export interface DocumentModel {
    id?: string;
    reference?: firebase.firestore.DocumentReference;
    get?();
}