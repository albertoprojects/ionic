import {Injectable} from '@angular/core';
import {FirebaseProvider} from './firebase';
import {Observable} from 'rxjs/Observable';
import {DocumentModel} from '../models/documentModel';
import {Query} from '../models/firebase/query';
import {GeoQuery} from '../models/firebase/geoquery';
import * as firebase from 'firebase';
import {AngularFirestore, DocumentChangeAction, AngularFirestoreDocument, Action } from 'angularfire2/firestore';
import * as geofirex  from'geofirex';

@Injectable()
export class DataProvider {

  constructor(private firebaseProvider: FirebaseProvider) {
    
  }

  getAll<T extends DocumentModel>(collectionName): Observable<T[]> {
    return this.firebaseProvider.getCollection(collectionName).map(elements => {
      return elements.map( element => {
          const auxElement: T =  element.payload.doc.data() as T;
          auxElement.id = element.payload.doc.id;
          auxElement.reference = element.payload.doc.ref;
          return auxElement; 
      });
    });
  }

  add<T extends DocumentModel>(collectionName, element: T): Promise<firebase.firestore.DocumentReference> {
    return this.firebaseProvider.addToCollection(collectionName, element).then(reference => {
      return Promise.resolve(reference);
    }).catch(error => {
      return Promise.reject(error);
    });
  }

  getDocument<T extends DocumentModel>(collection: string ,  reference: string): Observable<T> {
    return this.firebaseProvider.getDocument(collection, reference).map(element => {
      const auxElement: T = element.payload.data() as T
      auxElement.id = element.payload.id;
      auxElement.reference = element.payload.ref;
      return auxElement;
    });
  }

  getFirebaseDocument(collection, reference): Observable<Action<firebase.firestore.DocumentSnapshot>> {
     return this.firebaseProvider.getDocument(collection, reference);
  }


  updateDocument<T extends DocumentModel>(element) {
    
  }

  getByQuery<T extends DocumentModel>(collectionName: string, query: Query): Observable<T[]> {  
    return this.firebaseProvider.getCollectionByQuery(collectionName, query).map(elements => {
      return elements.map( element => {
          const auxElement: T =  element.payload.doc.data() as T;
          auxElement.id = element.payload.doc.id;
          auxElement.reference = element.payload.doc.ref;
          return auxElement; 
      });
    });
  }

  getByGeoQuery<T extends DocumentModel>(collectionName: string, geoquery: GeoQuery): Observable<T[]> {
    return this.firebaseProvider.getCollectionByGeoQuery(collectionName, geoquery).map(elements => {
      return elements.map( element => {
          const auxElement: T =  element.payload.doc.data() as T;
          auxElement.id = element.payload.doc.id;
          auxElement.reference = element.payload.doc.ref;
          return auxElement; 
      });
    });
  }

  getByQueryNoTransform(collectionName: string, query: Query): Observable<DocumentChangeAction[]> {  
    return this.firebaseProvider.getCollectionByQuery(collectionName, query).map(elements => {
      return elements;
    });
  }

  callFunction(name: string, params: any){
    var queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    return this.firebaseProvider.callFunction(name, queryString);
  }

  
} 