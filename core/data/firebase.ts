import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AngularFirestore, DocumentChangeAction, AngularFirestoreDocument, Action} from 'angularfire2/firestore';
import {FirebaseApp} from 'angularfire2';
import {Observable} from 'rxjs/Observable';
import {Query} from '../models/firebase/query';
import {GeoQuery} from '../models/firebase/geoquery';
import {Filter} from '../models/firebase/filter';
import * as firebase from 'firebase';
import * as geofirex from 'geofirex';

@Injectable()
export class FirebaseProvider {
  
  private geo: any;
  constructor(private db: AngularFirestore, 
    private firebaseApp: FirebaseApp,
    private httpClient: HttpClient) {
    const firestore = firebase.firestore();
    const settings = { timestampsInSnapshots: true};
    firestore.settings(settings);

    this.geo = geofirex.init(firebase);
  }

  getCollection(collectionName: string): Observable<DocumentChangeAction[]> {
    return this.db.collection(collectionName).snapshotChanges();
  }

  addToCollection(collectionName: string, element: {}): Promise<firebase.firestore.DocumentReference> {
    return this.db.collection(collectionName).add(element);
  }

  getDocument(collection: string, documentReference: string): Observable<Action<firebase.firestore.DocumentSnapshot>>{
    return this.db.collection(collection).doc(documentReference).snapshotChanges();
  }

  getCollectionByQuery(collectionName: string, query: Query): Observable<DocumentChangeAction[]> {
    return this.db.collection(collectionName, ref => 
      this.concatWhere(query, ref)
    ).snapshotChanges();
  }

  getCollectionByGeoQuery(collectionName: string, geoquery: GeoQuery): Observable<DocumentChangeAction[]> {
    const center = this.geo.point(geoquery.center.lat, geoquery.center.lng);
    return this.geo.collection(collectionName).within(center, geoquery.radius, geoquery.field);
  }

  concatWhere(query: Query, collection: firebase.firestore.CollectionReference): firebase.firestore.Query {
    var auxQuery = collection.where(query.filters[0].key, query.filters[0].condition,query.filters[0].value);
    for (var i = 1; i < query.filters.length; i++) {
      auxQuery = auxQuery.where(query.filters[i].key, query.filters[i].condition,query.filters[i].value);
    }
    if (query.orderBy) {
     auxQuery = auxQuery.orderBy(query.orderBy);
    }
    return auxQuery;  
  }


  callFunction(name : string, params: string): Promise<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Access-Control-Allow-Origin','*');
    headers = headers.set('Access-Control-Allow-Methods','PUT, GET, POST, DELETE, OPTIONS');
    headers = headers.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With: Content-Type, Accept, Authorization');

    return this.firebaseApp.auth().currentUser.getIdToken().then(idToken => {
      localStorage.setItem('AuthToken', idToken);
      const options = {};
      return this.httpClient.post('' + name + '?' + params, options).toPromise().then(response => {
        console.log('call ok' + response);
        Promise.resolve();  
      }, error=> {
        console.log('error call' + error);
        Promise.reject(error);
      });
    }).catch(error => {
       console.log('Error enter token' + error);
      Promise.reject(error);
    });
  }

  

}  